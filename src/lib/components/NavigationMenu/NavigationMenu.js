// Modules
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import get from "lodash.get";
// Styles
import styles from "./NavigationMenu.css";

class NavigationMenu extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    logo: PropTypes.node,
    style: PropTypes.shape({
      container: PropTypes.object,
      innerContainer: PropTypes.object,
      logoContainer: PropTypes.object
    })
  };

  render() {
    const { children, logo, style } = this.props;

    return (
      <div style={{ ...styles.container, ...get(style, "container", {}) }}>
        {logo && (
          <div
            style={{
              ...styles.logoContainer,
              ...get(style, "logoContainer", {})
            }}
          >
            {logo}
          </div>
        )}
        <div
          style={{
            ...styles.innerContainer,
            ...get(style, "innerContainer", {})
          }}
        >
          {children}
        </div>
      </div>
    );
  }
}

export default NavigationMenu;
