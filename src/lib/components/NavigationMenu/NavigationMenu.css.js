const styles = {
  container: {
    boxShadow: "0px 0px 24px rgba(0, 0, 0, 0.1)",
    display: "grid",
    justifyContent: "center",
    justifyItems: "center",
    gridAutoFlow: "row",
    gridGap: "48px 0px",
    gridTemplateRows: "auto 1fr",
    padding: "32px 0px",
    width: "300px"
  },
  innerContainer: {
    display: "flex",
    flexDirection: "column",
    paddingLeft: "24px"
  },
  logoContainer: {
    display: "flex",
    padding: "0px 24px"
  }
};

export default styles;
