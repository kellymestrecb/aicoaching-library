import Global from "../../../../config/global.css";

const styles = {
  icon: {
    color: Global.colors.darkSlateBlue,
    cursor: "pointer",
    fontSize: "24px"
  },
  message: {
    alignItems: "center",
    display: "flex",
    padding: 0
  },
  root: {
    backgroundColor: "transparent",
    boxShadow: "none",
    minWidth: "unset !important",
    width: "fit-content"
  }
};

export default styles;
