// Modules
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import { withStyles } from "@material-ui/core/styles";
// Components
import { InfoMessage } from "../../../";
import SnackbarContent from "@material-ui/core/SnackbarContent";
// Icons
import CloseIcon from "@material-ui/icons/Close";
// Styles
import styles from "./SnackbarContentWrapper.css";

class SnackbarContentWrapper extends PureComponent {
  static propTypes = {
    classes: PropTypes.object,
    className: PropTypes.string,
    CustomCloseIcon: PropTypes.func,
    message: PropTypes.string,
    onClose: PropTypes.func,
    type: PropTypes.oneOf(["error", "info", "success", "warn"])
  };

  render() {
    const {
      classes,
      className,
      CustomCloseIcon,
      message,
      onClose,
      type
    } = this.props;

    return (
      <SnackbarContent
        style={styles.root}
        className={clsx(classes.root, classes.message, className)}
        aria-describedby="client-snackbar"
        message={
          <InfoMessage
            endAdornment={
              CustomCloseIcon ? (
                <CustomCloseIcon
                  key="close"
                  aria-label="close"
                  onClick={onClose}
                />
              ) : (
                <CloseIcon
                  className={classes.icon}
                  key="close"
                  aria-label="close"
                  onClick={onClose}
                />
              )
            }
            icon={"default"}
            message={message}
            type={type}
          />
        }
      />
    );
  }
}

export default withStyles(styles)(SnackbarContentWrapper);
