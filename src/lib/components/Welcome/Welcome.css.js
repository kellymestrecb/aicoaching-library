import Global from "../../config/global.css";

const styles = {
  actionContainer: {
    alignItems: "flex-end",
    display: "flex",
    flex: 0,
    marginTop: "16px"
  },
  container: {
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    height: "100%",
    justifyContent: "center",
    margin: "72px 62px",
    minHeight: "min-content"
  },
  innerContainer: {
    alignItems: "center",
    display: "flex",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center"
  },
  logoContainer: {
    maxWidth: "240px"
  },
  messageContainer: {
    alignItems: "center",
    backgroundColor: Global.colors.starlightBlueLighter,
    borderRadius: "8px",
    color: Global.colors.darkNavyBlue,
    display: "flex",
    fontSize: "16px",
    fontStyle: "normal",
    fontWeight: "normal",
    lineHeight: "23px",
    padding: "24px",
    margin: "16px 0px",
    maxWidth: "560px",
    textAlign: "center"
  },
  titleContainer: {
    alignItems: "center",
    color: Global.colors.darkNavyBlue,
    fontSize: "21px",
    fontStyle: "normal",
    fontWeight: "600",
    lineHeight: "31px",
    margin: "32px 0px 0px",
    maxWidth: "240px",
    textAlign: "center"
  }
};

export default styles;
