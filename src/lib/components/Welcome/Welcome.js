// Modules
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import get from "lodash.get";
import clsx from "clsx";
// Styles
import styles from "./Welcome.css";

class Welcome extends PureComponent {
  static propTypes = {
    action: PropTypes.node.isRequired,
    defaultClassName: PropTypes.string,
    logo: PropTypes.node.isRequired,
    message: PropTypes.node,
    title: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
    style: PropTypes.shape({
      actionContainer: PropTypes.object,
      container: PropTypes.object,
      innerContainer: PropTypes.object,
      logoContainer: PropTypes.object,
      messageContainer: PropTypes.object,
      titleContainer: PropTypes.object
    })
  };

  render() {
    const {
      action,
      defaultClassName,
      logo,
      message,
      title,
      style
    } = this.props;

    return (
      <div
        className={clsx("welcome-container", defaultClassName)}
        style={{
          ...styles.container,
          ...get(style, "container", {})
        }}
      >
        <div
          className="welcome-inner-container"
          style={{
            ...styles.innerContainer,
            ...get(style, "innerContainer", {})
          }}
        >
          {logo && (
            <div
              className="welcome-logo-container"
              style={{
                ...styles.logoContainer,
                ...get(style, "logoContainer", {})
              }}
            >
              {logo}
            </div>
          )}
          {title && (
            <div
              className="welcome-title-container"
              style={{
                ...styles.titleContainer,
                ...get(style, "titleContainer", {})
              }}
            >
              {title}
            </div>
          )}
          {message && (
            <div
              className="welcome-message-container"
              style={{
                ...styles.messageContainer,
                ...get(style, "messageContainer", {})
              }}
            >
              {message}
            </div>
          )}
        </div>

        {action && (
          <div
            className="welcome-action-container"
            style={{
              ...styles.actionContainer,
              ...get(style, "actionContainer", {})
            }}
          >
            {action}
          </div>
        )}
      </div>
    );
  }
}

export default Welcome;
