const styles = {
  label: {
    fontSize: "12px",
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: "0.05em",
    lineHeight: "18px",
    paddingLeft: "20px",
    paddingRight: "20px",
    textTransform: "uppercase"
  },
  root: {
    alignItems: "center",
    borderRadius: "100px",
    height: "32px",
    textAlign: "center",
    width: "fit-content"
  }
};

export default styles;
