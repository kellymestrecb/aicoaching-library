// Modules
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import get from "lodash.get";
// Components
import { Chip } from "@material-ui/core";
// Styles
import styles from "./MaterialChip.css";

const CssChip = style => {
  let customStyle = { ...styles };
  customStyle.root = {
    ...get(customStyle, "root", {}),
    ...get(style, "root", {})
  };
  customStyle.root["&.Mui-disabled"] = {
    ...customStyle.root["&.Mui-disabled"],
    ...get(style, "disabled", {})
  };
  customStyle.label = {
    ...get(customStyle, "label", {}),
    ...get(style, "label", {})
  };
  customStyle.outlined = {
    ...get(customStyle, "outlined", {}),
    ...get(style, "outlined", {})
  };
  customStyle.clickable = {
    ...get(customStyle, "clickable", {}),
    ...get(style, "clickable", {})
  };

  return withStyles(customStyle)(Chip);
};

class MaterialChip extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    clickable: PropTypes.bool,
    disabled: PropTypes.bool,
    label: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
    size: PropTypes.oneOf(["medium", "small"]),
    style: PropTypes.shape({
      clickable: PropTypes.object,
      disabled: PropTypes.object,
      label: PropTypes.object,
      outlined: PropTypes.object,
      root: PropTypes.object
    }),
    variant: PropTypes.oneOf(["default", "outlined"])
  };

  static defaultProps = {
    clickable: false,
    disabled: false,
    size: "medium",
    variant: "default"
  };

  render() {
    const {
      className,
      clickable,
      disabled,
      label,
      size,
      style,
      variant
    } = this.props;

    const StyledChip = CssChip(style);

    return (
      <StyledChip
        className={className}
        clickable={clickable}
        disabled={disabled}
        label={label}
        size={size}
        variant={variant}
      />
    );
  }
}

export default MaterialChip;
