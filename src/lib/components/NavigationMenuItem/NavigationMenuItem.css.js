import Global from "../../config/global.css";

const styles = {
  active: {
    backgroundColor: Global.colors.lightPaleGrey,
    color: Global.colors.blue
  },
  container: {
    alignItems: "center",
    backgroundColor: "transparent",
    cursor: "pointer",
    display: "flex",
    width: "276px"
  },
  disabled: {
    backgroundColor: "transparent",
    color: Global.colors.paleGrey,
    cursor: "default"
  },
  iconContainer: {
    alignItems: "center",
    display: "flex",
    height: "24px",
    justifyContent: "center",
    width: "24px"
  },
  innerContainer: {
    alignItems: "center",
    borderRadius: "100px 0px 0px 100px",
    color: Global.colors.darkSlateBlue,
    display: "grid",
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "normal",
    gridAutoFlow: "column",
    gridGap: "0px 16px",
    gridTemplateColumns: "24px 1fr",
    lineHeight: "21px",
    padding: "20px 22px",
    width: "inherit"
  },
  onHover: {
    backgroundColor: Global.colors.lightPaleGrey,
    color: Global.colors.darkSlateBlue
  }
};

export default styles;
