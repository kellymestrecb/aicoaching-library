// Modules
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import get from "lodash.get";
// Styles
import styles from "./NavigationMenuItem.css";

class NavigationMenuItem extends PureComponent {
  static propTypes = {
    active: PropTypes.bool,
    disabled: PropTypes.bool,
    icon: PropTypes.node.isRequired,
    label: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
    onClick: PropTypes.func.isRequired,
    style: PropTypes.shape({
      active: PropTypes.object,
      container: PropTypes.object,
      disabled: PropTypes.object,
      iconContainer: PropTypes.object,
      innerContainer: PropTypes.object,
      labelContainer: PropTypes.object,
      onHover: PropTypes.object
    })
  };

  state = {
    onHover: false
  };

  handleMouseEnter = () => {
    this.setState({ onHover: true });
  };

  handleMouseLeave = () => {
    this.setState({ onHover: false });
  };

  render() {
    const { active, disabled, icon, label, onClick, style } = this.props;
    const { onHover } = this.state;

    return (
      <div
        onClick={onClick}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        style={{ ...styles.container, ...get(style, "container", {}) }}
      >
        <div
          style={{
            ...styles.innerContainer,
            ...get(style, "innerContainer", {}),
            ...(active
              ? { ...styles.active, ...get(style, "active", {}) }
              : {}),
            ...(onHover
              ? {
                  ...styles.onHover,
                  ...get(style, "onHover", {})
                }
              : {}),
            ...(disabled
              ? {
                  ...styles.disabled,
                  ...get(style, "disabled", {})
                }
              : {})
          }}
        >
          <div
            style={{
              ...styles.iconContainer,
              ...get(style, "iconContainer", {})
            }}
          >
            {icon}
          </div>
          <div style={get(style, "labelContainer", {})}>{label}</div>
        </div>
      </div>
    );
  }
}

export default NavigationMenuItem;
