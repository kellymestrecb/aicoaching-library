const styles = {
  container: {
    display: "grid",
    justifyContent: "center",
    gridGap: "16px 0px",
    margin: "20px"
  }
};

export default styles;
