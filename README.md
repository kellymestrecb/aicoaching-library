# AI Coaching Components Library

To install this library from other project use:

`npm install https://<your bitbucket user name>@bitbucket.org/kellymestrecb/aicoaching-library.git`

To use the libary components in your project:

`import { MaterialTextField } from "aicoaching-library";`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run check-lint`

Launches the linter check to raise the lint errors

### `npm run build`

Compiles the code and make them available to be imported from other projects.

### `npm run docz:dev`

Runs the documentation development server.
Open [http://localhost:4000](http://localhost:4000) to view it in the browser.

And there you can see all the components documentation and some examples of the usage.
