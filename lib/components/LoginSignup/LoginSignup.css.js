"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  container: {
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    height: "100%",
    justifyContent: "center",
    margin: "48px"
  },
  contentContainer: {
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center"
  },
  footerContainer: {
    alignItems: "flex-end",
    color: _global.default.colors.darkSlateBlue,
    display: "flex",
    flex: 1,
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "normal",
    lineHeight: "18px",
    marginTop: "48px",
    maxWidth: "240px"
  },
  inputsContainer: {
    margin: "22px 0px 32px"
  },
  logoContainer: {
    marginBottom: "112px",
    maxWidth: "240px"
  },
  subTitleContainer: {
    color: _global.default.colors.darkSlateBlue,
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "normal",
    lineHeight: "18px",
    margin: "15px 0px 32px",
    maxWidth: "240px",
    textAlign: "center"
  },
  titleContainer: {
    color: _global.default.colors.darkSlateBlue,
    fontSize: "24px",
    fontStyle: "normal",
    fontWeight: "normal",
    lineHeight: "30px",
    maxWidth: "240px",
    textAlign: "center"
  }
};
var _default = styles;
exports.default = _default;