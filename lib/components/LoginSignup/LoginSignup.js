"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _lodash = _interopRequireDefault(require("lodash.get"));

var _LoginSignup = _interopRequireDefault(require("./LoginSignup.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol["for"] && Symbol["for"]("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = { children: void 0 }; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var LoginSignup = /*#__PURE__*/function (_PureComponent) {
  _inherits(LoginSignup, _PureComponent);

  var _super = _createSuper(LoginSignup);

  function LoginSignup() {
    _classCallCheck(this, LoginSignup);

    return _super.apply(this, arguments);
  }

  _createClass(LoginSignup, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          action = _this$props.action,
          children = _this$props.children,
          errorMessage = _this$props.errorMessage,
          errorMessagePosition = _this$props.errorMessagePosition,
          footer = _this$props.footer,
          logo = _this$props.logo,
          title = _this$props.title,
          subTitle = _this$props.subTitle,
          style = _this$props.style;
      return /*#__PURE__*/_jsx("div", {
        className: "login-signup-container",
        style: _extends({}, _LoginSignup.default.container, (0, _lodash.default)(style, "container", {}))
      }, void 0, errorMessagePosition === "top" && errorMessage, /*#__PURE__*/_jsx("div", {
        className: "login-signup-content-container",
        style: _extends({}, _LoginSignup.default.contentContainer, (0, _lodash.default)(style, "contentContainer", {}))
      }, void 0, logo && /*#__PURE__*/_jsx("div", {
        style: _extends({}, _LoginSignup.default.logoContainer, (0, _lodash.default)(style, "logoContainer", {}))
      }, void 0, logo), title && /*#__PURE__*/_jsx("div", {
        className: "login-signup-title-container",
        style: _extends({}, _LoginSignup.default.titleContainer, (0, _lodash.default)(style, "titleContainer", {}))
      }, void 0, title), subTitle && /*#__PURE__*/_jsx("div", {
        className: "login-signup-subtitle-container",
        style: _extends({}, _LoginSignup.default.subTitleContainer, (0, _lodash.default)(style, "subTitleContainer", {}))
      }, void 0, subTitle), errorMessagePosition === "center" && errorMessage, children && /*#__PURE__*/_jsx("div", {
        className: "login-signup-children-container",
        style: _extends({}, _LoginSignup.default.inputsContainer, (0, _lodash.default)(style, "childrenContainer", {}))
      }, void 0, children)), action, footer && /*#__PURE__*/_jsx("div", {
        className: "login-signup-footer-container",
        style: _extends({}, _LoginSignup.default.footerContainer, (0, _lodash.default)(style, "footerContainer", {}))
      }, void 0, footer));
    }
  }]);

  return LoginSignup;
}(_react.PureComponent);

LoginSignup.defaultProps = {
  errorMessagePosition: "center"
};
process.env.NODE_ENV !== "production" ? LoginSignup.propTypes = {
  action: _propTypes.default.node.isRequired,
  children: _propTypes.default.node.isRequired,
  errorMessage: _propTypes.default.node,
  errorMessagePosition: _propTypes.default.oneOf(["center", "top"]),
  footer: _propTypes.default.node,
  logo: _propTypes.default.node,
  title: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),
  subTitle: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),
  style: _propTypes.default.shape({
    childrenContainer: _propTypes.default.object,
    container: _propTypes.default.object,
    contentContainer: _propTypes.default.object,
    footerContainer: _propTypes.default.object,
    logoContainer: _propTypes.default.object,
    subTitleContainer: _propTypes.default.object,
    titleContainer: _propTypes.default.object
  })
} : void 0;
var _default = LoginSignup;
exports.default = _default;