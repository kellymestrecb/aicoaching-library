"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _lodash = _interopRequireDefault(require("lodash.get"));

var _ = require("../");

var _Profile = _interopRequireDefault(require("./Profile.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol["for"] && Symbol["for"]("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = { children: void 0 }; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Profile = /*#__PURE__*/function (_PureComponent) {
  _inherits(Profile, _PureComponent);

  var _super = _createSuper(Profile);

  function Profile() {
    var _this;

    _classCallCheck(this, Profile);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.profileReference = undefined;
    _this.state = {
      anchorEl: undefined,
      isActive: false,
      onHover: false,
      isMenuOpen: false
    };

    _this.renderProfileImage = function () {
      var _this$props = _this.props,
          image = _this$props.image,
          style = _this$props.style,
          title = _this$props.title;
      var imageToRender = undefined;

      if (image) {
        if (typeof image === "string") {
          imageToRender = /*#__PURE__*/_jsx("img", {
            alt: "profile",
            style: _extends({}, _Profile.default.image, (0, _lodash.default)(style, "image", {})),
            src: image
          });
        } else {
          imageToRender = image;
        }
      } else {
        imageToRender = /*#__PURE__*/_jsx("div", {
          style: _extends({}, _Profile.default.image, _Profile.default.defaultImage, (0, _lodash.default)(style, "image", {}))
        }, void 0, title ? title[0].toUpperCase() : "U");
      }

      return /*#__PURE__*/_jsx("div", {
        style: _Profile.default.image
      }, void 0, imageToRender);
    };

    _this.iconComponent = function () {
      var _this$props2 = _this.props,
          iconClose = _this$props2.iconClose,
          iconOpen = _this$props2.iconOpen,
          style = _this$props2.style;
      var isMenuOpen = _this.state.isMenuOpen;
      var icon = isMenuOpen ? iconClose : iconOpen;
      var iconStyle = isMenuOpen ? "iconClose" : "iconOpen";

      var customIconStyle = _extends({}, _Profile.default.icon, (0, _lodash.default)(style, iconStyle, {}));

      if (typeof icon === "string") {
        return /*#__PURE__*/_jsx("span", {
          className: icon,
          style: _extends({}, customIconStyle)
        });
      } else {
        return /*#__PURE__*/_jsx("div", {
          style: _extends({}, customIconStyle)
        }, void 0, icon);
      }
    };

    _this.handleOnClick = function (event) {
      var _this$props3 = _this.props,
          onClick = _this$props3.onClick,
          settingsOptions = _this$props3.settingsOptions;
      settingsOptions && settingsOptions.length > 0 && _this.setState({
        anchorEl: event && event.currentTarget,
        isMenuOpen: false
      });
      onClick && onClick();
    };

    _this.handleMenuOpen = function () {
      var isMenuOpen = _this.state.isMenuOpen;
      !isMenuOpen && _this.setState({
        isMenuOpen: true
      });
    };

    _this.handleMouseEnter = function () {
      var onHover = _this.state.onHover;
      !onHover && _this.setState({
        onHover: true
      });
    };

    _this.handleMouseLeave = function () {
      var onHover = _this.state.onHover;
      onHover && _this.setState({
        onHover: false
      });
    };

    return _this;
  }

  _createClass(Profile, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props4 = this.props,
          activeSetting = _this$props4.activeSetting,
          isSelected = _this$props4.isSelected,
          menuProps = _this$props4.menuProps,
          settingsOptions = _this$props4.settingsOptions,
          subTitle = _this$props4.subTitle,
          style = _this$props4.style,
          title = _this$props4.title;
      var _this$state = this.state,
          anchorEl = _this$state.anchorEl,
          onHover = _this$state.onHover;
      var hasSettings = settingsOptions && settingsOptions.length > 0;
      return /*#__PURE__*/_jsx("div", {}, "profile-outercontainer", /*#__PURE__*/_react.default.createElement("div", {
        ref: function ref(_ref) {
          _this2.profileReference = _ref;
        },
        key: "profile-container",
        onClick: this.handleOnClick,
        onMouseEnter: this.handleMouseEnter,
        onMouseLeave: this.handleMouseLeave,
        style: _extends({}, _Profile.default.container, anchorEl || onHover || isSelected ? _Profile.default.containerActive : {}, anchorEl ? _Profile.default.containerExpanded : {}, hasSettings ? _Profile.default.containerClickable : {}, (0, _lodash.default)(style, "container", {}))
      }, this.renderProfileImage(), (title || subTitle) && /*#__PURE__*/_jsx("div", {
        style: _Profile.default.innerContainer
      }, "profile-innercontainer", title && /*#__PURE__*/_jsx("div", {
        style: _extends({}, _Profile.default.title, (0, _lodash.default)(style, "title", {}))
      }, "profile-title", title), subTitle && /*#__PURE__*/_jsx("div", {
        style: _extends({}, _Profile.default.subTitle, (0, _lodash.default)(style, "subTitle", {}))
      }, "profile-subtitle", subTitle)), this.iconComponent()), hasSettings && /*#__PURE__*/_jsx(ProfileMenu, {
        activeSetting: activeSetting,
        anchorEl: anchorEl,
        handleMenuOpen: this.handleMenuOpen,
        handleOnClick: this.handleOnClick,
        menuProps: menuProps,
        profileReference: this.profileReference,
        settingsOptions: settingsOptions
      }));
    }
  }]);

  return Profile;
}(_react.PureComponent);

process.env.NODE_ENV !== "production" ? Profile.propTypes = {
  activeSetting: _propTypes.default.object,

  /** The icon node or the icon class name */
  iconClose: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),
  iconOpen: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),

  /** The image node or the image already imported */
  image: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),

  /** For cases where one of the menu options is selected and there is a process logic on the main screen */
  isSelected: _propTypes.default.bool,
  menuProps: _propTypes.default.object,
  onClick: _propTypes.default.func,
  settingsOptions: _propTypes.default.arrayOf(_propTypes.default.shape({
    action: _propTypes.default.func,
    label: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),
    key: _propTypes.default.string,
    value: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.string])
  })),
  subTitle: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),
  title: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),
  style: _propTypes.default.shape({
    container: _propTypes.default.object,
    iconClose: _propTypes.default.object,
    iconOpen: _propTypes.default.object,
    image: _propTypes.default.object,
    subTitle: _propTypes.default.object,
    title: _propTypes.default.object
  })
} : void 0;

var ProfileMenu = /*#__PURE__*/function (_PureComponent2) {
  _inherits(ProfileMenu, _PureComponent2);

  var _super2 = _createSuper(ProfileMenu);

  function ProfileMenu() {
    _classCallCheck(this, ProfileMenu);

    return _super2.apply(this, arguments);
  }

  _createClass(ProfileMenu, [{
    key: "render",
    value: function render() {
      var _this$props5 = this.props,
          activeSetting = _this$props5.activeSetting,
          anchorEl = _this$props5.anchorEl,
          handleMenuOpen = _this$props5.handleMenuOpen,
          handleOnClick = _this$props5.handleOnClick,
          menuProps = _this$props5.menuProps,
          profileReference = _this$props5.profileReference,
          settingsOptions = _this$props5.settingsOptions;
      return /*#__PURE__*/_react.default.createElement(_.MaterialMenu, _extends({
        anchorEl: anchorEl,
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "center"
        },
        transformOrigin: {
          vertical: "top",
          horizontal: "center"
        },
        onClose: function onClose() {
          return handleOnClick();
        },
        onOpen: handleMenuOpen,
        options: settingsOptions,
        optionSelected: activeSetting,
        style: {
          container: {
            width: "".concat(profileReference && profileReference.clientWidth || 220, "px")
          }
        }
      }, menuProps));
    }
  }]);

  return ProfileMenu;
}(_react.PureComponent);

process.env.NODE_ENV !== "production" ? ProfileMenu.propTypes = {
  activeSetting: _propTypes.default.object,
  anchorEl: _propTypes.default.object,
  handleMenuOpen: _propTypes.default.func,
  handleOnClick: _propTypes.default.func,
  menuProps: _propTypes.default.object,
  profileReference: _propTypes.default.object,
  settingsOptions: _propTypes.default.array
} : void 0;
var _default = Profile;
exports.default = _default;