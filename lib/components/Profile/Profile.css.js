"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  container: {
    alignItems: "center",
    display: "grid",
    gridAutoFlow: "column",
    gridGap: "16px",
    maxWidth: "288px",
    padding: "12px 16px",
    width: "fit-content"
  },
  containerActive: {
    backgroundColor: _global.default.colors.white,
    borderRadius: "6px",
    boxShadow: "0px 0px 8px rgba(0, 0, 0, 0.1)"
  },
  containerClickable: {
    cursor: "pointer"
  },
  containerExpanded: {
    borderRadius: "6px 6px 0px 0px"
  },
  defaultImage: {
    alignItems: "center",
    backgroundColor: _global.default.colors.darkPaleGrey,
    color: _global.default.colors.white,
    display: "flex",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "18px",
    justifyContent: "center",
    lineHeight: "23px"
  },
  icon: {
    alignSelf: "center",
    display: "flex",
    justifySelf: "center",
    height: "24px",
    width: "24px"
  },
  image: {
    borderRadius: "100px",
    display: "flex",
    height: "48px",
    width: "48px"
  },
  innerContainer: {
    display: "grid",
    gridGap: "5px"
  },
  subTitle: {
    color: _global.default.colors.blueyGrey,
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "10px",
    lineHeight: "13px",
    overflow: "hidden",
    textOverflow: "ellipsis",
    textTransform: "uppercase",
    whiteSpace: "nowrap"
  },
  title: {
    color: _global.default.colors.darkSlateBlue,
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "14px",
    lineHeight: "18px",
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap"
  }
};
var _default = styles;
exports.default = _default;