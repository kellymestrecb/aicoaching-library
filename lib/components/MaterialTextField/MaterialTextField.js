"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("@material-ui/core/styles");

var _core = require("@material-ui/core");

var _MaterialTextField = _interopRequireDefault(require("./MaterialTextField.css"));

var _customStyleHelper = require("./helpers/customStyleHelper");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol["for"] && Symbol["for"]("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = { children: void 0 }; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var CssTextField = function CssTextField(variant, multiline, style) {
  var customStyle = (0, _customStyleHelper.formatStyle)(variant, multiline, _MaterialTextField.default, style);
  return (0, _styles.withStyles)(customStyle)(_core.TextField);
};

var MaterialTextField = /*#__PURE__*/function (_PureComponent) {
  _inherits(MaterialTextField, _PureComponent);

  var _super = _createSuper(MaterialTextField);

  function MaterialTextField() {
    var _this;

    _classCallCheck(this, MaterialTextField);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.state = {
      fontsLoaded: false
    };
    _this.StyledTextField = CssTextField(_this.props.variant, _this.props.multiline, _this.props.style);
    return _this;
  }

  _createClass(MaterialTextField, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var timeoutLoadFonts = this.props.timeoutLoadFonts;
      setTimeout(function () {
        _this2.setState({
          fontsLoaded: true
        });
      }, timeoutLoadFonts);
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props = this.props,
          children = _this$props.children,
          disabled = _this$props.disabled,
          endAdornment = _this$props.endAdornment,
          error = _this$props.error,
          errorMessage = _this$props.errorMessage,
          errorMessageStyle = _this$props.errorMessageStyle,
          input = _this$props.input,
          InputProps = _this$props.InputProps,
          label = _this$props.label,
          labelStyle = _this$props.labelStyle,
          meta = _this$props.meta,
          onChange = _this$props.onChange,
          select = _this$props.select,
          startAdornment = _this$props.startAdornment,
          style = _this$props.style,
          timeoutLoadFonts = _this$props.timeoutLoadFonts,
          type = _this$props.type,
          variant = _this$props.variant,
          props = _objectWithoutProperties(_this$props, ["children", "disabled", "endAdornment", "error", "errorMessage", "errorMessageStyle", "input", "InputProps", "label", "labelStyle", "meta", "onChange", "select", "startAdornment", "style", "timeoutLoadFonts", "type", "variant"]);

      var fontsLoaded = this.state.fontsLoaded;

      if (!fontsLoaded) {
        return null;
      }

      var customInputProps = _extends({
        endAdornment: endAdornment && /*#__PURE__*/_jsx(_core.InputAdornment, {
          position: "end"
        }, void 0, endAdornment),
        startAdornment: startAdornment && /*#__PURE__*/_jsx(_core.InputAdornment, {
          position: "start"
        }, void 0, startAdornment)
      }, InputProps);

      variant === "standard" && (customInputProps["disableUnderline"] = true);
      var hasError = error || Boolean(meta && meta.touched && meta.error);
      var helperTextErrorMessage = errorMessage || meta && meta.touched && meta.error;
      return /*#__PURE__*/_react.default.createElement(this.StyledTextField, _extends({
        disabled: disabled,
        error: hasError,
        FormHelperTextProps: {
          style: errorMessageStyle
        },
        helperText: hasError && helperTextErrorMessage,
        InputLabelProps: {
          ref: function ref(_ref) {
            if (!_this3.labelRef) {
              _this3.labelRef = _ref;

              _this3.forceUpdate();
            }
          },
          style: labelStyle
        },
        InputProps: customInputProps,
        label: label,
        onChange: onChange && onChange,
        select: select,
        style: _extends({}, _MaterialTextField.default.container, style),
        type: type,
        variant: variant
      }, input, props), children);
    }
  }]);

  return MaterialTextField;
}(_react.PureComponent);

MaterialTextField.defaultProps = {
  timeoutLoadFonts: 100,
  type: "text",
  variant: "standard"
};
process.env.NODE_ENV !== "production" ? MaterialTextField.propTypes = {
  children: _propTypes.default.node,
  disabled: _propTypes.default.bool,
  endAdornment: _propTypes.default.node,
  error: _propTypes.default.bool,
  errorMessage: _propTypes.default.string,
  errorMessageStyle: _propTypes.default.object,
  input: _propTypes.default.object,
  InputProps: _propTypes.default.object,
  label: _propTypes.default.string.isRequired,
  labelStyle: _propTypes.default.object,
  meta: _propTypes.default.object,
  multiline: _propTypes.default.bool,
  onChange: _propTypes.default.func,
  select: _propTypes.default.bool,
  startAdornment: _propTypes.default.node,
  style: _propTypes.default.shape({
    disabled: _propTypes.default.object,
    error: _propTypes.default.shape({
      input: _propTypes.default.object,
      label: _propTypes.default.object
    }),
    focused: _propTypes.default.object,
    focusedInput: _propTypes.default.object,
    helperText: _propTypes.default.object,
    input: _propTypes.default.object,
    inputRoot: _propTypes.default.object,
    label: _propTypes.default.object,
    labelShrinked: _propTypes.default.object
  }),
  timeoutLoadFonts: _propTypes.default.number,
  type: _propTypes.default.oneOf(["email", "number", "password", "text"]),
  value: _propTypes.default.oneOfType([_propTypes.default.number, _propTypes.default.string]),
  variant: _propTypes.default.oneOf(["outlined", "standard"])
} : void 0;
var _default = MaterialTextField;
exports.default = _default;