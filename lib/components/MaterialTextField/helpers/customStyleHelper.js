"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatStyle = formatStyle;

var _lodash = _interopRequireDefault(require("lodash.get"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function formatStyle(variant, multiline, styles, style) {
  var customStyle = (0, _lodash.default)(styles, variant, {});

  var errorStyle = _extends({}, (0, _lodash.default)(style, "error", {}));

  delete errorStyle.input;
  delete errorStyle.label;
  var focusedStyle = (0, _lodash.default)(style, "focused", {});
  var focusedInputStyle = (0, _lodash.default)(style, "focusedInput", {});
  var disabledStyle = (0, _lodash.default)(style, "disabled", {});
  customStyle.root["& label"] = _extends({}, customStyle.root["& label"], (0, _lodash.default)(style, "label", {}));
  customStyle.root["& label"]["&.Mui-error"] = _extends({}, customStyle.root["& label"]["&.Mui-error"], errorStyle);
  customStyle.root["& label"]["&.Mui-error"] = _extends({}, customStyle.root["& label"]["&.Mui-error"], (0, _lodash.default)(style, "error.label", {}));
  customStyle.root["& label"]["&.Mui-disabled"] = _extends({}, customStyle.root["& label"]["&.Mui-disabled"], disabledStyle);
  customStyle.root["& label"]["&.Mui-focused"] = _extends({}, customStyle.root["& label"]["&.Mui-focused"], focusedStyle);
  customStyle.root["& .MuiInputBase-input"] = _extends({}, customStyle.root["& .MuiInputBase-input"], (0, _lodash.default)(style, "input", {}));
  customStyle.root["& .MuiInputBase-input"]["&.Mui-disabled"] = _extends({}, customStyle.root["& .MuiInputBase-input"]["&.Mui-disabled"], disabledStyle);
  customStyle.root["& .MuiInputBase-root"] = _extends({}, customStyle.root["& .MuiInputBase-root"], (0, _lodash.default)(style, "inputRoot", {}));
  customStyle.root["& .MuiInputBase-root"]["&.Mui-error"] = _extends({}, customStyle.root["& .MuiInputBase-root"]["&.Mui-error"], (0, _lodash.default)(style, "error.input", {}));
  customStyle.root["& .MuiFormHelperText-root"] = _extends({}, customStyle.root["& .MuiFormHelperText-root"], (0, _lodash.default)(style, "helperText", {}));
  customStyle.root["& .MuiFormHelperText-root"]["&.Mui-error"] = _extends({}, customStyle.root["& .MuiFormHelperText-root"]["&.Mui-error"], errorStyle);

  if (variant === "outlined") {
    customStyle.root["& .MuiInputLabel-outlined"]["&.MuiInputLabel-shrink"] = _extends({}, customStyle.root["& .MuiInputLabel-outlined"]["&.MuiInputLabel-shrink"], (0, _lodash.default)(style, "labelShrinked", {}));
    customStyle.root["& .MuiOutlinedInput-notchedOutline"] = _extends({}, customStyle.root["& .MuiOutlinedInput-notchedOutline"], (0, _lodash.default)(style, "outline", {}));
    customStyle.root["& .MuiOutlinedInput-root"]["&.Mui-error"]["& .MuiOutlinedInput-notchedOutline"] = _extends({}, customStyle.root["& .MuiOutlinedInput-root"]["&.Mui-error"]["& .MuiOutlinedInput-notchedOutline"], errorStyle);
    customStyle.root["& .MuiOutlinedInput-root"]["&.Mui-focused"] = _extends({}, customStyle.root["& .MuiOutlinedInput-root"]["&.Mui-focused"], focusedInputStyle);
    customStyle.root["& .MuiOutlinedInput-root"]["&.Mui-focused"]["& .MuiOutlinedInput-notchedOutline"] = _extends({}, customStyle.root["& .MuiOutlinedInput-root"]["&.Mui-focused"]["& .MuiOutlinedInput-notchedOutline"], focusedStyle);
    customStyle.root["& .MuiOutlinedInput-root"]["&.Mui-disabled"] = _extends({}, customStyle.root["& .MuiOutlinedInput-root"]["&.Mui-disabled"], disabledStyle);
    multiline && (customStyle.root["& .MuiOutlinedInput-multiline"] = _extends({}, customStyle.root["& .MuiOutlinedInput-multiline"], (0, _lodash.default)(style, "input", {})));
  }

  return customStyle;
}