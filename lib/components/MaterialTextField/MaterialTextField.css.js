"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  container: {
    width: "240px"
  },
  outlined: {
    root: {
      "& label": {
        color: _global.default.colors.darkSlateBlue,
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "normal",
        letterSpacing: "unset",
        lineHeight: "18px",
        "&.Mui-disabled": {
          color: _global.default.colors.darkPaleGrey
        },
        "&.Mui-error": {
          color: "".concat(_global.default.colors.orange, " !important")
        },
        "&.Mui-focused": {
          color: _global.default.colors.darkSlateBlue
        }
      },
      "& .MuiFormHelperText-contained": {
        margin: "0px 12px"
      },
      "& .MuiFormHelperText-root": {
        fontSize: "10px",
        fontStyle: "normal",
        fontWeight: "normal",
        letterSpacing: "unset",
        lineHeight: "13px",
        "&.Mui-error": {
          color: _global.default.colors.orange
        }
      },
      "& .MuiInputBase-input": {
        color: "inherit",
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "normal",
        letterSpacing: "unset",
        lineHeight: "18px",
        "&.Mui-disabled": {
          color: _global.default.colors.darkPaleGrey
        }
      },
      "& .MuiInputBase-root": {
        color: _global.default.colors.darkSlateBlue,
        height: "48px"
      },
      "& .MuiInputBase-multiline": {
        height: "unset !important",
        padding: "8px 4px 8px 0px"
      },
      "& .MuiInputLabel-outlined": {
        transform: "translate(12px, 15px) scale(1)",
        "&.MuiInputLabel-shrink": {
          transform: "translate(12px, -6px) scale(0.75)"
        }
      },
      "& .MuiOutlinedInput-root": {
        "&.Mui-disabled": {
          borderColor: _global.default.colors.darkPaleGrey,
          borderWidth: "1px"
        },
        "&.Mui-error": {
          "& .MuiOutlinedInput-notchedOutline": {
            borderColor: _global.default.colors.orange,
            borderWidth: "1px"
          }
        },
        "&.Mui-focused": {
          "& .MuiOutlinedInput-notchedOutline": {
            borderColor: _global.default.colors.darkSlateBlue,
            borderWidth: "1px"
          }
        }
      },
      "& .MuiOutlinedInput-input": {
        padding: "15px 10px"
      },
      "& .MuiOutlinedInput-inputMultiline": {
        padding: "7px 10px !important"
      },
      "& .MuiOutlinedInput-notchedOutline": {
        borderColor: _global.default.colors.darkSlateBlue,
        borderRadius: "8px",
        borderWidth: "1px",
        paddingLeft: "10px !important",
        top: "-6px",
        "& legend": {
          height: "13px !important"
        },
        "& legend span": {
          fontSize: "0.64rem !important",
          paddingLeft: "1px !important",
          paddingRight: "2px !important"
        }
      }
    }
  },
  standard: {
    root: {
      "& label": {
        color: _global.default.colors.darkSlateBlue,
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "normal",
        letterSpacing: "unset",
        lineHeight: "18px",
        "&.Mui-disabled": {
          color: _global.default.colors.darkPaleGrey
        },
        "&.Mui-error": {
          color: "".concat(_global.default.colors.orange, " !important")
        },
        "&.Mui-focused": {
          color: _global.default.colors.darkSlateBlue
        }
      },
      "& label + .MuiInput-formControl": {
        height: "48px",
        marginTop: "unset"
      },
      "& .MuiFormHelperText-root": {
        fontSize: "10px",
        fontStyle: "normal",
        fontWeight: "normal",
        letterSpacing: "unset",
        lineHeight: "13px",
        marginTop: "unset",
        "&.Mui-error": {
          color: _global.default.colors.orange
        }
      },
      "& .MuiInputBase-root": {
        color: _global.default.colors.darkSlateBlue,
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "normal",
        letterSpacing: "unset",
        lineHeight: "18px",
        "&.Mui-disabled": {
          color: _global.default.colors.darkPaleGrey
        }
      },
      "& .MuiInputBase-input": {
        padding: "20px 0px 10px"
      }
    }
  }
};
var _default = styles;
exports.default = _default;