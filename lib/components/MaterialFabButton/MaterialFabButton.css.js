"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  endAdornment: {
    display: "flex",
    marginLeft: "4px"
  },
  extended: {
    root: {
      "& .MuiFab-label": {
        justifyContent: "space-evenly"
      },
      "&.MuiFab-root": {
        backgroundColor: _global.default.colors.blue,
        borderRadius: "100px",
        boxShadow: "none",
        color: _global.default.colors.white,
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "normal",
        height: "48px",
        letterSpacing: "unset",
        lineHeight: "18px",
        textTransform: "unset",
        "&.Mui-disabled": {
          color: "rgba(0, 0, 0, 0.26)",
          backgroundColor: "rgba(0, 0, 0, 0.12)"
        },
        "&:hover": {
          backgroundColor: "#0034AC"
        },
        "&:active": {
          backgroundColor: _global.default.colors.blue
        }
      }
    }
  },
  round: {
    root: {
      "&.MuiFab-root": {
        backgroundColor: _global.default.colors.blue,
        boxShadow: "none",
        color: _global.default.colors.white,
        height: "48px",
        width: "48px",
        "&.Mui-disabled": {
          color: "rgba(0, 0, 0, 0.26)",
          backgroundColor: "rgba(0, 0, 0, 0.12)"
        },
        "&:hover": {
          backgroundColor: "#0034AC"
        },
        "&:active": {
          backgroundColor: _global.default.colors.blue
        }
      }
    }
  },
  startAdornment: {
    display: "flex",
    marginRight: "4px"
  }
};
var _default = styles;
exports.default = _default;