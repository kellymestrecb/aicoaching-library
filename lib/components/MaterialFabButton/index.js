"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _MaterialFabButton = _interopRequireDefault(require("./MaterialFabButton"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = _MaterialFabButton.default;
exports.default = _default;