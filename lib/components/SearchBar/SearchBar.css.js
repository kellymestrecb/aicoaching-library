"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  boxShadowForContainer: {
    boxShadow: "0px 0px 8px rgba(0, 0, 0, 0.1)"
  },
  container: {
    background: _global.default.colors.white,
    border: "1px solid transparent",
    borderRadius: "100px",
    cursor: "pointer",
    display: "grid",
    height: "48px",
    gridAutoFlow: "column",
    gridGap: "16px",
    gridTemplateColumns: "1fr auto",
    paddingLeft: "24px",
    paddingRight: "16px"
  },
  disabled: {
    background: _global.default.colors.white,
    color: _global.default.colors.darkPaleGrey,
    cursor: "default"
  },
  focus: {
    color: _global.default.colors.darkSlateBlue
  },
  icon: {
    alignItems: "center",
    color: _global.default.colors.darkSlateBlue,
    display: "flex",
    fontSize: "24px",
    justifyContent: "center"
  },
  input: {
    border: "none",
    color: _global.default.colors.darkSlateBlue,
    cursor: "pointer",
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "normal",
    height: "max-content",
    lineHeight: "18px",
    outline: "none",
    padding: "14px 0"
  },
  onHover: {
    border: "1px solid ".concat(_global.default.colors.paleGrey)
  },
  selected: {
    color: _global.default.colors.darkSlateBlue
  }
};
var _default = styles;
exports.default = _default;