"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _lodash = _interopRequireDefault(require("lodash.get"));

var _SearchBar = _interopRequireDefault(require("./SearchBar.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol["for"] && Symbol["for"]("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = { children: void 0 }; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var SearchBar = /*#__PURE__*/function (_PureComponent) {
  _inherits(SearchBar, _PureComponent);

  var _super = _createSuper(SearchBar);

  function SearchBar() {
    var _this;

    _classCallCheck(this, SearchBar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.state = {
      isFocus: false,
      onHover: undefined,
      value: ""
    };

    _this.handleOnChange = function (e) {
      var onChange = _this.props.onChange;
      var value = e.target.value;

      if (onChange) {
        _this.setState({
          value: value
        }, onChange(value));
      } else {
        _this.setState({
          value: value
        });
      }
    };

    _this.handleOnFocus = function (value) {
      return function () {
        _this.setState({
          isFocus: value
        });
      };
    };

    _this.iconComponent = function (icon) {
      var _this$props = _this.props,
          disabled = _this$props.disabled,
          style = _this$props.style;
      var value = _this.state.value;
      var isFocus = _this.state.isFocus;

      if (typeof icon === "string") {
        return /*#__PURE__*/_jsx("span", {
          className: icon,
          style: _extends({}, _SearchBar.default.icon, (0, _lodash.default)(style, "icon", {}), isFocus ? _SearchBar.default.focus : {}, value && isFocus ? _SearchBar.default.selected : {}, disabled ? _SearchBar.default.disabled : {})
        });
      } else if (icon) {
        return /*#__PURE__*/_jsx("div", {
          style: _extends({}, _SearchBar.default.icon, (0, _lodash.default)(style, "icon", {}), isFocus ? _SearchBar.default.focus : {}, value && isFocus ? _SearchBar.default.selected : {}, disabled ? _SearchBar.default.disabled : {})
        }, void 0, icon);
      } else {
        return null;
      }
    };

    _this.handleOnHover = function () {
      _this.setState({
        onHover: true
      });
    };

    _this.handleOnBlur = function () {
      _this.setState({
        onHover: false
      });
    };

    return _this;
  }

  _createClass(SearchBar, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          disabled = _this$props2.disabled,
          icon = _this$props2.icon,
          inputClassName = _this$props2.inputClassName,
          inputValue = _this$props2.inputValue,
          placeholder = _this$props2.placeholder,
          style = _this$props2.style;
      var _this$state = this.state,
          isFocus = _this$state.isFocus,
          onHover = _this$state.onHover;
      var inputClass = inputClassName ? "input ".concat(inputClassName) : "input";
      return /*#__PURE__*/_jsx("div", {
        onMouseEnter: this.handleOnHover,
        onMouseLeave: this.handleOnBlur,
        style: _extends({}, _SearchBar.default.container, (0, _lodash.default)(style, "container", {}), onHover && !disabled ? _SearchBar.default.onHover : {}, isFocus ? _SearchBar.default.boxShadowForContainer : {}, disabled ? _SearchBar.default.disabled : {})
      }, void 0, /*#__PURE__*/_jsx("input", {
        className: inputClass,
        disabled: disabled,
        onChange: this.handleOnChange,
        onFocus: this.handleOnFocus(true),
        onBlur: this.handleOnFocus(false),
        placeholder: placeholder,
        style: _extends({}, _SearchBar.default.input, (0, _lodash.default)(style, "input", {}), disabled ? _SearchBar.default.disabled : {}),
        value: inputValue
      }), this.iconComponent(icon));
    }
  }]);

  return SearchBar;
}(_react.PureComponent);

process.env.NODE_ENV !== "production" ? SearchBar.propTypes = {
  disabled: _propTypes.default.bool,
  icon: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),
  inputClassName: _propTypes.default.string,
  inputValue: _propTypes.default.string,
  onChange: _propTypes.default.func,
  placeholder: _propTypes.default.string,
  style: _propTypes.default.shape({
    container: _propTypes.default.object,
    icon: _propTypes.default.object,
    input: _propTypes.default.object
  })
} : void 0;
var _default = SearchBar;
exports.default = _default;