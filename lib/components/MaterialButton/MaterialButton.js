"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _styles = require("@material-ui/core/styles");

var _lodash = _interopRequireDefault(require("lodash.get"));

var _Button = _interopRequireDefault(require("@material-ui/core/Button"));

var _MaterialButton = _interopRequireDefault(require("./MaterialButton.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol["for"] && Symbol["for"]("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = { children: void 0 }; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var CssButton = function CssButton(customType, style) {
  var customStyle = {
    root: (0, _lodash.default)(_MaterialButton.default, "root", {})
  };
  customStyle.root = _extends({}, customStyle.root, (0, _lodash.default)(_MaterialButton.default, customType, {}), (0, _lodash.default)(style, "root", {}));
  customStyle.root["&:hover"] = _extends({}, customStyle.root["&:hover"], (0, _lodash.default)(style, "onHover", {}));
  customStyle.root["&:active"] = _extends({}, customStyle.root["&:active"], (0, _lodash.default)(style, "active", {}));
  return (0, _styles.withStyles)(customStyle)(_Button.default);
};

var MaterialButton = /*#__PURE__*/function (_PureComponent) {
  _inherits(MaterialButton, _PureComponent);

  var _super = _createSuper(MaterialButton);

  function MaterialButton() {
    _classCallCheck(this, MaterialButton);

    return _super.apply(this, arguments);
  }

  _createClass(MaterialButton, [{
    key: "render",
    value: function render() {
      var _classNames;

      var _this$props = this.props,
          children = _this$props.children,
          classes = _this$props.classes,
          containerStyle = _this$props.containerStyle,
          customType = _this$props.customType,
          defaultClassName = _this$props.defaultClassName,
          disabled = _this$props.disabled,
          disableRipple = _this$props.disableRipple,
          onClick = _this$props.onClick,
          onMouseEnter = _this$props.onMouseEnter,
          onMouseLeave = _this$props.onMouseLeave,
          style = _this$props.style,
          type = _this$props.type;
      var className = (0, _classnames.default)(classes.root, defaultClassName, (_classNames = {}, _defineProperty(_classNames, classes.default, customType.includes("default")), _defineProperty(_classNames, classes.link, customType.includes("link")), _defineProperty(_classNames, classes.boldPrimary, customType.includes("boldPrimary")), _classNames));
      var StyledButton = CssButton(customType, style);
      return /*#__PURE__*/_jsx("div", {
        style: containerStyle
      }, void 0, /*#__PURE__*/_jsx(StyledButton, {
        disableRipple: disableRipple,
        className: className,
        disabled: disabled,
        onClick: onClick,
        onMouseEnter: onMouseEnter,
        onMouseLeave: onMouseLeave,
        style: style,
        variant: "text",
        type: type
      }, void 0, children));
    }
  }]);

  return MaterialButton;
}(_react.PureComponent);

MaterialButton.defaultProps = {
  customType: "default",
  type: "button"
};
process.env.NODE_ENV !== "production" ? MaterialButton.propTypes = {
  children: _propTypes.default.node.isRequired,
  classes: _propTypes.default.object,
  containerStyle: _propTypes.default.object,
  customType: _propTypes.default.oneOf(["boldPrimary", "default", "link"]),
  defaultClassName: _propTypes.default.string,
  disabled: _propTypes.default.bool,
  disableRipple: _propTypes.default.bool,
  onClick: _propTypes.default.func.isRequired,
  onMouseEnter: _propTypes.default.func,
  onMouseLeave: _propTypes.default.func,
  style: _propTypes.default.shape({
    active: _propTypes.default.object,
    onHover: _propTypes.default.object,
    root: _propTypes.default.object
  }),
  type: _propTypes.default.oneOf(["button", "submit"])
} : void 0;

var _default = (0, _styles.withStyles)(_MaterialButton.default)(MaterialButton);

exports.default = _default;