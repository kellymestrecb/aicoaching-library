"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  boldPrimary: {
    backgroundColor: _global.default.colors.oliveGreen,
    borderRadius: "12px 12px 0px 12px",
    boxShadow: "none",
    color: _global.default.colors.white,
    "&:hover": {
      backgroundColor: "#6FAA34",
      boxShadow: "none"
    },
    "&:active": {
      boxShadow: "none"
    }
  },
  disabled: {
    backgroundColor: "transparent",
    boxShadow: "none",
    color: "".concat(_global.default.colors.paleGrey, " !important")
  },
  link: {
    backgroundColor: "transparent",
    boxShadow: "none",
    color: _global.default.colors.darkSlateBlue,
    minHeight: "auto !important",
    minWidth: "auto !important",
    padding: "0 !important",
    textDecorationLine: "underline",
    width: "auto !important",
    "&:hover": {
      backgroundColor: "transparent",
      boxShadow: "none",
      color: _global.default.colors.blue,
      textDecorationLine: "underline"
    },
    "&:active": {
      backgroundColor: "transparent",
      boxShadow: "none",
      color: _global.default.colors.blue,
      textDecorationLine: "underline"
    }
  },
  root: {
    fontSize: "14px",
    fontStretch: "normal",
    fontStyle: "normal",
    fontWeight: "normal",
    lineHeight: "18px",
    maxHeight: "48px",
    minWidth: "140px",
    padding: "16px 24px",
    textTransform: "none",
    width: "100%"
  }
};
var _default = styles;
exports.default = _default;