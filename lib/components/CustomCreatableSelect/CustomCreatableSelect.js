"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _lodash = _interopRequireDefault(require("lodash.get"));

var _reactSelect = require("react-select");

var _creatable = _interopRequireDefault(require("react-select/creatable"));

var _core = require("@material-ui/core");

var _icons = require("@material-ui/icons");

var _CustomCreatableSelect = _interopRequireDefault(require("./CustomCreatableSelect.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol["for"] && Symbol["for"]("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = { children: void 0 }; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var CustomCreatableSelect = /*#__PURE__*/function (_PureComponent) {
  _inherits(CustomCreatableSelect, _PureComponent);

  var _super = _createSuper(CustomCreatableSelect);

  function CustomCreatableSelect() {
    var _this;

    _classCallCheck(this, CustomCreatableSelect);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _this.ClearIndicator = function (props) {
      var _props$innerProps = props.innerProps,
          ref = _props$innerProps.ref,
          restInnerProps = _objectWithoutProperties(_props$innerProps, ["ref"]);

      return /*#__PURE__*/_react.default.createElement("div", _extends({}, restInnerProps, {
        style: _CustomCreatableSelect.default.iconContainer,
        ref: ref
      }), /*#__PURE__*/_jsx(_icons.Close, {
        style: _CustomCreatableSelect.default.clearIcon
      }));
    };

    _this.MultiValueRemove = function (props) {
      return /*#__PURE__*/_react.default.createElement(_reactSelect.components.MultiValueRemove, props, /*#__PURE__*/_jsx(_icons.Close, {
        style: _CustomCreatableSelect.default.iconRemoveContainer
      }));
    };

    return _this;
  }

  _createClass(CustomCreatableSelect, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          defaultOption = _this$props.defaultOption,
          input = _this$props.input;
      input && defaultOption && input.onChange(defaultOption);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          containerStyle = _this$props2.containerStyle,
          controlStyle = _this$props2.controlStyle,
          customComponents = _this$props2.customComponents,
          defaultOption = _this$props2.defaultOption,
          disabled = _this$props2.disabled,
          disabledStyle = _this$props2.disabledStyle,
          editable = _this$props2.editable,
          error = _this$props2.error,
          errorMessage = _this$props2.errorMessage,
          errorMessageStyle = _this$props2.errorMessageStyle,
          input = _this$props2.input,
          inputStyle = _this$props2.inputStyle,
          isClearable = _this$props2.isClearable,
          isMulti = _this$props2.isMulti,
          menuListStyle = _this$props2.menuListStyle,
          menuStyle = _this$props2.menuStyle,
          meta = _this$props2.meta,
          multiValueLabelStyle = _this$props2.multiValueLabelStyle,
          multiValueRemoveStyle = _this$props2.multiValueRemoveStyle,
          multiValueStyle = _this$props2.multiValueStyle,
          _noOptionsMessage = _this$props2.noOptionsMessage,
          _onBlur = _this$props2.onBlur,
          _onMenuClose = _this$props2.onMenuClose,
          onSelection = _this$props2.onSelection,
          optionFocusedStyle = _this$props2.optionFocusedStyle,
          options = _this$props2.options,
          optionSelectedStyle = _this$props2.optionSelectedStyle,
          optionStyle = _this$props2.optionStyle,
          outerContainerStyle = _this$props2.outerContainerStyle,
          placeholder = _this$props2.placeholder,
          singleValueStyle = _this$props2.singleValueStyle,
          valueContainerStyle = _this$props2.valueContainerStyle,
          props = _objectWithoutProperties(_this$props2, ["containerStyle", "controlStyle", "customComponents", "defaultOption", "disabled", "disabledStyle", "editable", "error", "errorMessage", "errorMessageStyle", "input", "inputStyle", "isClearable", "isMulti", "menuListStyle", "menuStyle", "meta", "multiValueLabelStyle", "multiValueRemoveStyle", "multiValueStyle", "noOptionsMessage", "onBlur", "onMenuClose", "onSelection", "optionFocusedStyle", "options", "optionSelectedStyle", "optionStyle", "outerContainerStyle", "placeholder", "singleValueStyle", "valueContainerStyle"]);

      var _valueContainer = valueContainerStyle ? valueContainerStyle : null;

      var hasError = error || Boolean(meta && meta.touched && meta.error);
      var helperTextErrorMessage = errorMessage || meta && meta.touched && meta.error;
      return /*#__PURE__*/_jsx("div", {
        style: outerContainerStyle
      }, "custom-creatable-select-outer-container-".concat(props.keyLabel), /*#__PURE__*/_react.default.createElement(_creatable.default, _extends({
        components: {
          ClearIndicator: (0, _lodash.default)(customComponents, "ClearIndicator", this.ClearIndicator),
          DropdownIndicator: (0, _lodash.default)(customComponents, "DropdownIndicator", null),
          IndicatorSeparator: (0, _lodash.default)(customComponents, "IndicatorSeparator", null),
          MultiValueRemove: (0, _lodash.default)(customComponents, "MultiValueRemove", this.MultiValueRemove)
        },
        defaultValue: defaultOption,
        isDisabled: disabled,
        isClearable: isClearable,
        isMulti: isMulti,
        key: "custom-creatable-select-".concat(props.keyLabel),
        noOptionsMessage: function noOptionsMessage() {
          return _noOptionsMessage;
        },
        onBlur: function onBlur() {
          _onBlur && _onBlur();
        },
        onChange: function onChange(e) {
          onSelection && onSelection(e);
          input && input.onChange(e);
        },
        onMenuClose: function onMenuClose() {
          _onMenuClose && _onMenuClose();
        },
        options: options,
        placeholder: placeholder,
        styles: {
          container: function container(base) {
            return _extends({}, base, containerStyle);
          },
          control: function control(base, data) {
            return _extends({}, base, _CustomCreatableSelect.default.selectContainer, data ? _CustomCreatableSelect.default.selectContainerMargin : {}, data.isMulti ? _CustomCreatableSelect.default.selectContainerMulti : {}, disabled ? _CustomCreatableSelect.default.disabledContainer : {}, hasError ? _CustomCreatableSelect.default.errorContainer : {}, controlStyle, disabled ? _extends({}, _CustomCreatableSelect.default.disabledContainer, disabledStyle) : {});
          },
          input: function input(base) {
            return _extends({}, base, _CustomCreatableSelect.default.input, inputStyle);
          },
          menu: function menu(base) {
            return _extends({}, base, options ? _CustomCreatableSelect.default.menu : _CustomCreatableSelect.default.menuDisabled, menuStyle);
          },
          menuList: function menuList(base) {
            return _extends({}, base, _CustomCreatableSelect.default.menuList, menuListStyle);
          },
          multiValue: function multiValue(base) {
            return _extends({}, base, _CustomCreatableSelect.default.multiValue, multiValueStyle);
          },
          multiValueLabel: function multiValueLabel(base) {
            return _extends({}, base, _CustomCreatableSelect.default.multiValueLabel, multiValueLabelStyle);
          },
          multiValueRemove: function multiValueRemove(base) {
            return _extends({}, base, _CustomCreatableSelect.default.multiValueRemove, multiValueRemoveStyle, !(!disabled && editable) ? _CustomCreatableSelect.default.multiValueRemoveHide : {});
          },
          noOptionsMessage: function noOptionsMessage(base) {
            return _extends({}, base, _CustomCreatableSelect.default.noOptionsMessage);
          },
          option: function option(base, data) {
            return _extends({}, base, _CustomCreatableSelect.default.option, optionStyle, data.isSelected ? _extends({}, _CustomCreatableSelect.default.optionSelected, optionSelectedStyle) : {}, data.isFocused ? _extends({}, _CustomCreatableSelect.default.optionFocused, optionFocusedStyle) : {});
          },
          placeholder: function placeholder(base) {
            return _extends({}, base, _CustomCreatableSelect.default.placeholder);
          },
          singleValue: function singleValue(base) {
            return _extends({}, base, singleValueStyle);
          },
          valueContainer: function valueContainer(base) {
            return _extends({}, base, _CustomCreatableSelect.default.valueContainer, _valueContainer);
          }
        }
      }, props)), hasError && helperTextErrorMessage && /*#__PURE__*/_jsx(_core.FormHelperText, {
        error: true,
        id: "ccs-error-message",
        style: _extends({}, _CustomCreatableSelect.default.errorMessage, errorMessageStyle)
      }, "css-helper-text-".concat(props.keyLabel), helperTextErrorMessage));
    }
  }]);

  return CustomCreatableSelect;
}(_react.PureComponent);

process.env.NODE_ENV !== "production" ? CustomCreatableSelect.propTypes = {
  containerStyle: _propTypes.default.object,
  controlStyle: _propTypes.default.object,
  customComponents: _propTypes.default.shape({
    ClearIndicator: _propTypes.default.func,
    DropdownIndicator: _propTypes.default.func,
    IndicatorSeparator: _propTypes.default.func,
    MultiValueRemove: _propTypes.default.func
  }),
  defaultOption: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]),
  disabled: _propTypes.default.bool,
  disabledStyle: _propTypes.default.object,
  editable: _propTypes.default.bool,
  error: _propTypes.default.bool,
  errorMessage: _propTypes.default.string,
  errorMessageStyle: _propTypes.default.object,
  input: _propTypes.default.object,
  inputStyle: _propTypes.default.object,
  isClearable: _propTypes.default.bool,
  isMulti: _propTypes.default.bool,
  menuListStyle: _propTypes.default.object,
  menuStyle: _propTypes.default.object,
  meta: _propTypes.default.object,
  multiValueLabelStyle: _propTypes.default.object,
  multiValueRemoveStyle: _propTypes.default.object,
  multiValueStyle: _propTypes.default.object,
  noOptionsMessage: _propTypes.default.string,
  onBlur: _propTypes.default.func,
  onMenuClose: _propTypes.default.func,
  onSelection: _propTypes.default.func,
  optionFocusedStyle: _propTypes.default.object,
  options: _propTypes.default.array,
  optionSelectedStyle: _propTypes.default.object,
  optionStyle: _propTypes.default.object,
  outerContainerStyle: _propTypes.default.object,
  placeholder: _propTypes.default.string,
  singleValueStyle: _propTypes.default.object,
  valueContainerStyle: _propTypes.default.object
} : void 0;
var _default = CustomCreatableSelect;
exports.default = _default;