"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _lodash = _interopRequireDefault(require("lodash.get"));

var _styles = require("@material-ui/core/styles");

var _core = require("@material-ui/core");

var _MaterialMenu = _interopRequireDefault(require("./MaterialMenu.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol["for"] && Symbol["for"]("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = { children: void 0 }; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var CssMenu = function CssMenu(style) {
  var customStyle = (0, _lodash.default)(_MaterialMenu.default, "menu", {});
  customStyle.paper = _extends({}, customStyle.paper, {
    "&.MuiPopover-paper": (0, _lodash.default)(style, "container", {})
  }, {
    "& .MuiList-padding": _extends({}, customStyle.paper["& .MuiList-padding"], (0, _lodash.default)(style, "optionsContainer", {}))
  });
  return (0, _styles.withStyles)(customStyle)(_core.Menu);
};

var CssMenuItem = function CssMenuItem(style) {
  var customStyle = (0, _lodash.default)(_MaterialMenu.default, "menuItem", {});
  customStyle.root = _extends({}, customStyle.root, (0, _lodash.default)(style, "options", {}));
  return (0, _styles.withStyles)(customStyle)(_core.MenuItem);
};

var MaterialMenu = /*#__PURE__*/function (_PureComponent) {
  _inherits(MaterialMenu, _PureComponent);

  var _super = _createSuper(MaterialMenu);

  function MaterialMenu() {
    var _this;

    _classCallCheck(this, MaterialMenu);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.state = {
      anchorEl: undefined
    };

    _this.handleSelectOption = function (option) {
      var onClose = _this.props.onClose;

      _this.setState({
        selectedOption: option
      }, function () {
        option && option.action && option.action();
        onClose && onClose();
      });
    };

    return _this;
  }

  _createClass(MaterialMenu, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          anchorEl = _this$props.anchorEl,
          onClose = _this$props.onClose,
          onOpen = _this$props.onOpen,
          options = _this$props.options,
          optionSelected = _this$props.optionSelected,
          style = _this$props.style,
          props = _objectWithoutProperties(_this$props, ["anchorEl", "onClose", "onOpen", "options", "optionSelected", "style"]);

      if (!options || options.length === 0) {
        return null;
      }

      var StyledMenu = CssMenu(style);
      var StyledMenuItem = CssMenuItem(style);
      return /*#__PURE__*/_react.default.createElement(StyledMenu, _extends({
        anchorEl: anchorEl,
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "left"
        },
        getContentAnchorEl: null,
        keepMounted: true,
        onClose: onClose,
        onEnter: onOpen,
        open: Boolean(anchorEl),
        transformOrigin: {
          vertical: "top",
          horizontal: "left"
        }
      }, props), options.map(function (option) {
        return /*#__PURE__*/_jsx(StyledMenuItem, {
          onClick: function onClick() {
            return _this2.handleSelectOption(option);
          },
          selected: optionSelected && optionSelected.key === option.key
        }, option.key, option.value);
      }));
    }
  }]);

  return MaterialMenu;
}(_react.PureComponent);

process.env.NODE_ENV !== "production" ? MaterialMenu.propTypes = {
  anchorEl: _propTypes.default.object,
  anchorOrigin: _propTypes.default.shape({
    horizontal: _propTypes.default.string,
    vertical: _propTypes.default.string
  }),
  transformOrigin: _propTypes.default.shape({
    horizontal: _propTypes.default.string,
    vertical: _propTypes.default.string
  }),
  onClose: _propTypes.default.func.isRequired,
  onOpen: _propTypes.default.func.isRequired,
  options: _propTypes.default.arrayOf(_propTypes.default.shape({
    action: _propTypes.default.func,
    key: _propTypes.default.string,
    value: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.string])
  })),
  optionSelected: _propTypes.default.object,
  style: _propTypes.default.shape({
    container: _propTypes.default.object,
    options: _propTypes.default.object,
    optionsContainer: _propTypes.default.object
  })
} : void 0;
var _default = MaterialMenu;
exports.default = _default;