"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  menu: {
    paper: {
      boxShadow: "0px 0px 8px rgba(0, 0, 0, 0.1)",
      "&.MuiPaper-rounded": {
        borderRadius: "0px 0px 6px 6px"
      },
      "& .MuiList-padding": {
        padding: "0px"
      }
    }
  },
  menuItem: {
    root: {
      alignItems: "center",
      color: _global.default.colors.darkSlateBlue,
      fontSize: "10px",
      fontStyle: "normal",
      fontWeight: "normal",
      justifyContent: "center",
      lineHeight: "13px",
      minHeight: "64px",
      textTransform: "uppercase",
      whiteSpace: "normal",
      "&:hover": {
        backgroundColor: _global.default.colors.lightPaleGrey
      }
    }
  }
};
var _default = styles;
exports.default = _default;