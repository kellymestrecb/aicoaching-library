"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  container: {
    alignItems: "center",
    borderRadius: "4px",
    default: {
      backgroundColor: _global.default.colors.white
    },
    display: "flex",
    error: {
      backgroundColor: _global.default.colors.paleOrange
    },
    maxWidth: "400px",
    minHeight: "32px",
    padding: "12px 16px",
    success: {
      backgroundColor: _global.default.colors.paleGreen
    },
    warn: {
      backgroundColor: _global.default.colors.paleYellow
    },
    width: "fit-content"
  },
  icon: {
    fontSize: "15px",
    height: "15px",
    margin: "4px",
    width: "15px",
    success: {
      height: "24px",
      margin: "0px",
      width: "24px"
    }
  },
  innerContainer: {
    alignItems: "center",
    display: "grid",
    gridAutoFlow: "column",
    gridGap: "12px"
  },
  message: {
    color: _global.default.colors.darkSlateBlue,
    fontStyle: "normal",
    fontSize: "14px",
    fontWeight: "normal",
    lineHeight: "21px"
  }
};
var _default = styles;
exports.default = _default;