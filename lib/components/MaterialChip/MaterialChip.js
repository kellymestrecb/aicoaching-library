"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("@material-ui/core/styles");

var _lodash = _interopRequireDefault(require("lodash.get"));

var _core = require("@material-ui/core");

var _MaterialChip = _interopRequireDefault(require("./MaterialChip.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol["for"] && Symbol["for"]("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = { children: void 0 }; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var CssChip = function CssChip(style) {
  var customStyle = _extends({}, _MaterialChip.default);

  customStyle.root = _extends({}, (0, _lodash.default)(customStyle, "root", {}), (0, _lodash.default)(style, "root", {}));
  customStyle.root["&.Mui-disabled"] = _extends({}, customStyle.root["&.Mui-disabled"], (0, _lodash.default)(style, "disabled", {}));
  customStyle.label = _extends({}, (0, _lodash.default)(customStyle, "label", {}), (0, _lodash.default)(style, "label", {}));
  customStyle.outlined = _extends({}, (0, _lodash.default)(customStyle, "outlined", {}), (0, _lodash.default)(style, "outlined", {}));
  customStyle.clickable = _extends({}, (0, _lodash.default)(customStyle, "clickable", {}), (0, _lodash.default)(style, "clickable", {}));
  return (0, _styles.withStyles)(customStyle)(_core.Chip);
};

var MaterialChip = /*#__PURE__*/function (_PureComponent) {
  _inherits(MaterialChip, _PureComponent);

  var _super = _createSuper(MaterialChip);

  function MaterialChip() {
    _classCallCheck(this, MaterialChip);

    return _super.apply(this, arguments);
  }

  _createClass(MaterialChip, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          className = _this$props.className,
          clickable = _this$props.clickable,
          disabled = _this$props.disabled,
          label = _this$props.label,
          size = _this$props.size,
          style = _this$props.style,
          variant = _this$props.variant;
      var StyledChip = CssChip(style);
      return /*#__PURE__*/_jsx(StyledChip, {
        className: className,
        clickable: clickable,
        disabled: disabled,
        label: label,
        size: size,
        variant: variant
      });
    }
  }]);

  return MaterialChip;
}(_react.PureComponent);

MaterialChip.defaultProps = {
  clickable: false,
  disabled: false,
  size: "medium",
  variant: "default"
};
process.env.NODE_ENV !== "production" ? MaterialChip.propTypes = {
  className: _propTypes.default.string,
  clickable: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  label: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),
  size: _propTypes.default.oneOf(["medium", "small"]),
  style: _propTypes.default.shape({
    clickable: _propTypes.default.object,
    disabled: _propTypes.default.object,
    label: _propTypes.default.object,
    outlined: _propTypes.default.object,
    root: _propTypes.default.object
  }),
  variant: _propTypes.default.oneOf(["default", "outlined"])
} : void 0;
var _default = MaterialChip;
exports.default = _default;