"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var styles = {
  label: {
    fontSize: "12px",
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: "0.05em",
    lineHeight: "18px",
    paddingLeft: "20px",
    paddingRight: "20px",
    textTransform: "uppercase"
  },
  root: {
    alignItems: "center",
    borderRadius: "100px",
    height: "32px",
    textAlign: "center",
    width: "fit-content"
  }
};
var _default = styles;
exports.default = _default;