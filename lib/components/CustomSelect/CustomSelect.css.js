"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  arrowIcon: {
    color: _global.default.colors.darkSlateBlue,
    fontSize: "24px"
  },
  clearIcon: {
    color: _global.default.colors.blueyGrey,
    fontSize: "16px"
  },
  container: {
    height: "48px"
  },
  disabledContainer: {
    background: "transparent",
    border: "1px solid ".concat(_global.default.colors.paleGrey)
  },
  disabledIcon: {
    color: _global.default.colors.paleGrey
  },
  errorContainer: {
    border: "1px solid ".concat(_global.default.colors.orange)
  },
  errorMessage: {
    color: "".concat(_global.default.colors.orange),
    fontSize: "10px",
    fontStyle: "normal",
    fontWeight: "normal",
    letterSpacing: "unset",
    lineHeight: "13px",
    marginTop: 0,
    paddingLeft: "16px"
  },
  iconContainer: {
    alignItems: "center",
    color: _global.default.colors.darkSlateBlue,
    cursor: "pointer",
    display: "flex",
    height: "24px",
    justifyContent: "center",
    marginLeft: "16px",
    width: "24px"
  },
  iconRemoveContainer: {
    alignItems: "center",
    cursor: "pointer",
    display: "flex",
    height: "16px",
    justifyContent: "center",
    width: "16px"
  },
  input: {
    color: _global.default.colors.darkSlateBlue,
    fontSize: "10px",
    fontStyle: "normal",
    fontWeight: "600",
    lineHeight: "13px",
    marginLeft: 0
  },
  menu: {
    borderRadius: "8px",
    boxShadow: "0px 0px 8px rgba(0, 0, 0, 0.16)",
    margin: "4px 0 0"
  },
  menuList: {
    padding: 0
  },
  multiValue: {
    backgroundColor: _global.default.colors.lightPaleGrey,
    border: "1px solid ".concat(_global.default.colors.darkPaleGrey),
    color: _global.default.colors.darkSlateBlue,
    fontSize: "10px",
    fontStyle: "normal",
    fontWeight: "normal",
    lineHeight: "13px",
    margin: "4px 8px 4px 0px"
  },
  multiValueLabel: {
    alignItems: "center",
    color: _global.default.colors.darkSlateBlue,
    display: "flex",
    fontSize: "10px",
    fontStyle: "normal",
    fontWeight: "600",
    lineHeight: "13px",
    padding: "4px 4px",
    paddingLeft: "4px",
    textAlign: "center"
  },
  multiValueRemove: {
    alignItems: "center",
    borderRadius: "2px",
    color: _global.default.colors.darkSlateBlue,
    display: "flex",
    height: "24px",
    justifyContent: "center",
    margin: "-1px",
    padding: 0,
    width: "24px",
    "&:hover": {
      background: _global.default.colors.melon,
      color: _global.default.colors.white
    }
  },
  multiValueRemoveHide: {
    display: "none"
  },
  noOptionComponent: {
    height: "48px"
  },
  noOptionsMessage: {
    color: _global.default.colors.darkPaleGrey,
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "normal",
    lineHeight: "21px",
    height: "48px",
    padding: "15px 16px",
    textAlign: "start"
  },
  option: {
    textAlign: "start",
    color: _global.default.colors.darkSlateBlue,
    cursor: "pointer",
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "normal",
    height: "48px",
    lineHeight: "21px",
    padding: "15px 16px",
    wordBreak: "break-word",
    "&:active": {
      backgroundColor: _global.default.colors.blue,
      color: _global.default.colors.white
    },
    "&:first-of-type": {
      borderRadius: "8px 8px 0px 0px"
    },
    "&:last-child": {
      borderRadius: "0px 0px 8px 8px"
    },
    "&:first-of-type&:last-child": {
      borderRadius: "8px"
    }
  },
  optionFocused: {
    backgroundColor: _global.default.colors.lightPaleGrey,
    color: _global.default.colors.darkSlateBlue
  },
  optionSelected: {
    backgroundColor: _global.default.colors.blue,
    color: _global.default.colors.white
  },
  placeholder: {
    color: _global.default.colors.darkSlateBlue,
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "normal",
    lineHeight: "18px",
    marginLeft: 0
  },
  selectContainer: {
    backgroundColor: "transparent",
    border: "1px solid ".concat(_global.default.colors.darkSlateBlue),
    borderRadius: "8px",
    boxShadow: "unset",
    boxSizing: "border-box",
    height: "48px",
    padding: "15px 16px",
    placeContent: "center",
    "&:focus-within": {
      border: "1px solid ".concat(_global.default.colors.blue)
    },
    "&:hover": {
      cursor: "pointer"
    }
  },
  selectContainerMargin: {
    padding: "11px 16px"
  },
  selectContainerMulti: {
    height: "unset",
    minHeight: "48px"
  },
  selectContainerOpened: {
    borderRadius: "8px"
  },
  singleValue: {
    color: _global.default.colors.darkSlateBlue,
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "normal",
    lineHeight: "18px",
    marginLeft: 0
  },
  valueContainer: {
    padding: 0
  }
};
var _default = styles;
exports.default = _default;