"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _lodash = _interopRequireDefault(require("lodash.get"));

var _reactSelect = _interopRequireWildcard(require("react-select"));

var _icons = require("@material-ui/icons");

var _core = require("@material-ui/core");

var _CustomSelect = _interopRequireDefault(require("./CustomSelect.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol["for"] && Symbol["for"]("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = { children: void 0 }; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var CustomSelect = /*#__PURE__*/function (_PureComponent) {
  _inherits(CustomSelect, _PureComponent);

  var _super = _createSuper(CustomSelect);

  function CustomSelect() {
    var _this;

    _classCallCheck(this, CustomSelect);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _this.ClearIndicator = function (props) {
      var _props$innerProps = props.innerProps,
          ref = _props$innerProps.ref,
          restInnerProps = _objectWithoutProperties(_props$innerProps, ["ref"]);

      return /*#__PURE__*/_react.default.createElement("div", _extends({}, restInnerProps, {
        style: _CustomSelect.default.iconContainer,
        ref: ref
      }), /*#__PURE__*/_jsx(_icons.Close, {
        style: _CustomSelect.default.clearIcon
      }));
    };

    _this.DropdownIndicator = function (props) {
      var _props$innerProps2 = props.innerProps,
          ref = _props$innerProps2.ref,
          restInnerProps = _objectWithoutProperties(_props$innerProps2, ["ref"]);

      var _this$props = _this.props,
          disabled = _this$props.disabled,
          disabledStyle = _this$props.disabledStyle,
          dropdownIndicatorContainerStyle = _this$props.dropdownIndicatorContainerStyle;
      var icon = _this.props.icon;
      return /*#__PURE__*/_react.default.createElement("div", _extends({}, restInnerProps, {
        style: _extends({}, _CustomSelect.default.iconContainer, dropdownIndicatorContainerStyle),
        ref: ref
      }), icon ? icon : props.selectProps.menuIsOpen ? /*#__PURE__*/_jsx(_icons.ArrowDropUp, {
        style: _CustomSelect.default.arrowIcon
      }) : /*#__PURE__*/_jsx(_icons.ArrowDropDown, {
        style: disabled && disabledStyle ? _extends({}, _CustomSelect.default.arrowIcon, disabledStyle) : disabled ? _CustomSelect.default.disabledIcon : _CustomSelect.default.arrowIcon
      }));
    };

    _this.MultiValueRemove = function (props) {
      return /*#__PURE__*/_react.default.createElement(_reactSelect.components.MultiValueRemove, props, /*#__PURE__*/_jsx(_icons.Close, {
        style: _CustomSelect.default.iconRemoveContainer
      }));
    };

    return _this;
  }

  _createClass(CustomSelect, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props2 = this.props,
          defaultOption = _this$props2.defaultOption,
          input = _this$props2.input;
      input && input.onChange && defaultOption && input.onChange(defaultOption);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props3 = this.props,
          containerStyle = _this$props3.containerStyle,
          controlStyle = _this$props3.controlStyle,
          customComponents = _this$props3.customComponents,
          defaultOption = _this$props3.defaultOption,
          disabled = _this$props3.disabled,
          disabledStyle = _this$props3.disabledStyle,
          editable = _this$props3.editable,
          error = _this$props3.error,
          errorMessage = _this$props3.errorMessage,
          errorMessageStyle = _this$props3.errorMessageStyle,
          hideSelectedOptions = _this$props3.hideSelectedOptions,
          input = _this$props3.input,
          inputStyle = _this$props3.inputStyle,
          isClearable = _this$props3.isClearable,
          isMulti = _this$props3.isMulti,
          isSearchable = _this$props3.isSearchable,
          menuContainerStyle = _this$props3.menuContainerStyle,
          menuListContainerStyle = _this$props3.menuListContainerStyle,
          meta = _this$props3.meta,
          multiValueLabelStyle = _this$props3.multiValueLabelStyle,
          multiValueRemoveStyle = _this$props3.multiValueRemoveStyle,
          multiValueStyle = _this$props3.multiValueStyle,
          _noOptionsMessage = _this$props3.noOptionsMessage,
          _onBlur = _this$props3.onBlur,
          _onMenuClose = _this$props3.onMenuClose,
          onSelection = _this$props3.onSelection,
          optionFocusedStyle = _this$props3.optionFocusedStyle,
          options = _this$props3.options,
          optionSelectedStyle = _this$props3.optionSelectedStyle,
          optionStyle = _this$props3.optionStyle,
          outerContainerStyle = _this$props3.outerContainerStyle,
          placeholder = _this$props3.placeholder,
          singleValueStyle = _this$props3.singleValueStyle,
          value = _this$props3.value,
          valueContainerStyle = _this$props3.valueContainerStyle,
          props = _objectWithoutProperties(_this$props3, ["containerStyle", "controlStyle", "customComponents", "defaultOption", "disabled", "disabledStyle", "editable", "error", "errorMessage", "errorMessageStyle", "hideSelectedOptions", "input", "inputStyle", "isClearable", "isMulti", "isSearchable", "menuContainerStyle", "menuListContainerStyle", "meta", "multiValueLabelStyle", "multiValueRemoveStyle", "multiValueStyle", "noOptionsMessage", "onBlur", "onMenuClose", "onSelection", "optionFocusedStyle", "options", "optionSelectedStyle", "optionStyle", "outerContainerStyle", "placeholder", "singleValueStyle", "value", "valueContainerStyle"]);

      var _singleValue = singleValueStyle ? singleValueStyle : null;

      var _valueContainer = valueContainerStyle ? valueContainerStyle : null;

      var hasError = error || Boolean(meta && meta.touched && meta.error);
      var helperTextErrorMessage = errorMessage || meta && meta.touched && meta.error;
      return /*#__PURE__*/_jsx("div", {
        style: outerContainerStyle
      }, "custom-select-outer-container-".concat(props.keyLabel), /*#__PURE__*/_react.default.createElement(_reactSelect.default, _extends({
        closeMenuOnSelect: !isMulti,
        components: {
          ClearIndicator: (0, _lodash.default)(customComponents, "ClearIndicator", this.ClearIndicator),
          DropdownIndicator: (0, _lodash.default)(customComponents, "DropdownIndicator", this.DropdownIndicator),
          IndicatorSeparator: (0, _lodash.default)(customComponents, "IndicatorSeparator", null),
          MultiValueRemove: (0, _lodash.default)(customComponents, "MultiValueRemove", this.MultiValueRemove)
        },
        defaultValue: defaultOption,
        hideSelectedOptions: hideSelectedOptions !== undefined ? hideSelectedOptions : false,
        isClearable: isClearable || false,
        isDisabled: disabled,
        isMulti: isMulti || false,
        isSearchable: isSearchable || false,
        key: "custom-select-".concat(props.keyLabel),
        noOptionsMessage: function noOptionsMessage() {
          return _noOptionsMessage;
        },
        onBlur: function onBlur() {
          _onBlur && _onBlur();
        },
        onChange: function onChange(e) {
          onSelection && onSelection(e);
          input && input.onChange(e);
        },
        onMenuClose: function onMenuClose() {
          _onMenuClose && _onMenuClose();
        },
        options: options,
        placeholder: placeholder,
        styles: {
          container: function container(base) {
            return _extends({}, base, containerStyle);
          },
          control: function control(base, data) {
            return _extends({}, base, _CustomSelect.default.selectContainer, data ? _CustomSelect.default.selectContainerMargin : {}, data.isMulti ? _CustomSelect.default.selectContainerMulti : {}, data.isFocused && data.menuIsOpen ? _CustomSelect.default.selectContainerOpened : {}, hasError ? _CustomSelect.default.errorContainer : {}, controlStyle, disabled ? _extends({}, _CustomSelect.default.disabledContainer, disabledStyle) : {});
          },
          input: function input(base) {
            return _extends({}, base, _CustomSelect.default.input, inputStyle);
          },
          menu: function menu(base) {
            return _extends({}, base, _CustomSelect.default.menu, menuContainerStyle);
          },
          menuList: function menuList(base) {
            return _extends({}, base, _CustomSelect.default.menuList, menuListContainerStyle);
          },
          multiValue: function multiValue(base) {
            return _extends({}, base, _CustomSelect.default.multiValue, multiValueStyle);
          },
          multiValueLabel: function multiValueLabel(base) {
            return _extends({}, base, _CustomSelect.default.multiValueLabel, multiValueLabelStyle);
          },
          multiValueRemove: function multiValueRemove(base) {
            return _extends({}, base, _CustomSelect.default.multiValueRemove, multiValueRemoveStyle, !(!disabled && editable) ? _CustomSelect.default.multiValueRemoveHide : {});
          },
          noOptionsMessage: function noOptionsMessage(base) {
            return _extends({}, base, _CustomSelect.default.noOptionsMessage);
          },
          option: function option(base, data) {
            return _extends({}, base, _CustomSelect.default.option, optionStyle, data.isSelected ? _extends({}, _CustomSelect.default.optionSelected, optionSelectedStyle) : {}, data.isFocused ? _extends({}, _CustomSelect.default.optionFocused, optionFocusedStyle) : {});
          },
          placeholder: function placeholder(base) {
            return _extends({}, base, _CustomSelect.default.placeholder);
          },
          singleValue: function singleValue(base) {
            return _extends({}, base, _CustomSelect.default.singleValue, _singleValue);
          },
          valueContainer: function valueContainer(base) {
            return _extends({}, base, _CustomSelect.default.valueContainer, _valueContainer);
          }
        },
        value: value
      }, props)), hasError && helperTextErrorMessage && /*#__PURE__*/_jsx(_core.FormHelperText, {
        error: true,
        id: "cs-error-message",
        style: _extends({}, _CustomSelect.default.errorMessage, errorMessageStyle)
      }, "cs-helper-text-".concat(props.keyLabel), helperTextErrorMessage));
    }
  }]);

  return CustomSelect;
}(_react.PureComponent);

process.env.NODE_ENV !== "production" ? CustomSelect.propTypes = {
  containerStyle: _propTypes.default.object,
  controlStyle: _propTypes.default.object,
  customComponents: _propTypes.default.shape({
    ClearIndicator: _propTypes.default.func,
    DropdownIndicator: _propTypes.default.func,
    IndicatorSeparator: _propTypes.default.func,
    MultiValueRemove: _propTypes.default.func
  }),
  defaultOption: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]),
  disabled: _propTypes.default.bool,
  disabledStyle: _propTypes.default.object,
  dropdownIndicatorContainerStyle: _propTypes.default.object,
  editable: _propTypes.default.bool,
  error: _propTypes.default.bool,
  errorMessage: _propTypes.default.string,
  errorMessageStyle: _propTypes.default.object,
  hideSelectedOptions: _propTypes.default.bool,
  icon: _propTypes.default.node,
  input: _propTypes.default.object,
  inputStyle: _propTypes.default.object,
  isClearable: _propTypes.default.bool,
  isMulti: _propTypes.default.bool,
  isSearchable: _propTypes.default.bool,
  menuContainerStyle: _propTypes.default.object,
  menuListContainerStyle: _propTypes.default.object,
  meta: _propTypes.default.object,
  multiValueLabelStyle: _propTypes.default.object,
  multiValueRemoveStyle: _propTypes.default.object,
  multiValueStyle: _propTypes.default.object,
  name: _propTypes.default.string,
  noOptionsMessage: _propTypes.default.string,
  onBlur: _propTypes.default.func,
  onMenuClose: _propTypes.default.func,
  onSelection: _propTypes.default.func,
  optionFocusedStyle: _propTypes.default.object,
  options: _propTypes.default.arrayOf(_propTypes.default.shape({
    label: _propTypes.default.string.isRequired,
    value: _propTypes.default.string.isRequired,
    option: _propTypes.default.object.isRequired
  })),
  optionSelectedStyle: _propTypes.default.object,
  optionStyle: _propTypes.default.object,
  outerContainerStyle: _propTypes.default.object,
  placeholder: _propTypes.default.string,
  singleValueStyle: _propTypes.default.object,
  value: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]),
  valueContainerStyle: _propTypes.default.object
} : void 0;
var _default = CustomSelect;
exports.default = _default;