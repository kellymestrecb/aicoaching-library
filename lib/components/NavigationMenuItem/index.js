"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _NavigationMenuItem = _interopRequireDefault(require("./NavigationMenuItem"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = _NavigationMenuItem.default;
exports.default = _default;