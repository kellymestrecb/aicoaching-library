"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _lodash = _interopRequireDefault(require("lodash.get"));

var _NavigationMenuItem = _interopRequireDefault(require("./NavigationMenuItem.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol["for"] && Symbol["for"]("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = { children: void 0 }; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var NavigationMenuItem = /*#__PURE__*/function (_PureComponent) {
  _inherits(NavigationMenuItem, _PureComponent);

  var _super = _createSuper(NavigationMenuItem);

  function NavigationMenuItem() {
    var _this;

    _classCallCheck(this, NavigationMenuItem);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.state = {
      onHover: false
    };

    _this.handleMouseEnter = function () {
      _this.setState({
        onHover: true
      });
    };

    _this.handleMouseLeave = function () {
      _this.setState({
        onHover: false
      });
    };

    return _this;
  }

  _createClass(NavigationMenuItem, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          active = _this$props.active,
          disabled = _this$props.disabled,
          icon = _this$props.icon,
          label = _this$props.label,
          onClick = _this$props.onClick,
          style = _this$props.style;
      var onHover = this.state.onHover;
      return /*#__PURE__*/_jsx("div", {
        onClick: onClick,
        onMouseEnter: this.handleMouseEnter,
        onMouseLeave: this.handleMouseLeave,
        style: _extends({}, _NavigationMenuItem.default.container, (0, _lodash.default)(style, "container", {}))
      }, void 0, /*#__PURE__*/_jsx("div", {
        style: _extends({}, _NavigationMenuItem.default.innerContainer, (0, _lodash.default)(style, "innerContainer", {}), active ? _extends({}, _NavigationMenuItem.default.active, (0, _lodash.default)(style, "active", {})) : {}, onHover ? _extends({}, _NavigationMenuItem.default.onHover, (0, _lodash.default)(style, "onHover", {})) : {}, disabled ? _extends({}, _NavigationMenuItem.default.disabled, (0, _lodash.default)(style, "disabled", {})) : {})
      }, void 0, /*#__PURE__*/_jsx("div", {
        style: _extends({}, _NavigationMenuItem.default.iconContainer, (0, _lodash.default)(style, "iconContainer", {}))
      }, void 0, icon), /*#__PURE__*/_jsx("div", {
        style: (0, _lodash.default)(style, "labelContainer", {})
      }, void 0, label)));
    }
  }]);

  return NavigationMenuItem;
}(_react.PureComponent);

process.env.NODE_ENV !== "production" ? NavigationMenuItem.propTypes = {
  active: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  icon: _propTypes.default.node.isRequired,
  label: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]).isRequired,
  onClick: _propTypes.default.func.isRequired,
  style: _propTypes.default.shape({
    active: _propTypes.default.object,
    container: _propTypes.default.object,
    disabled: _propTypes.default.object,
    iconContainer: _propTypes.default.object,
    innerContainer: _propTypes.default.object,
    labelContainer: _propTypes.default.object,
    onHover: _propTypes.default.object
  })
} : void 0;
var _default = NavigationMenuItem;
exports.default = _default;