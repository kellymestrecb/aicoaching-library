"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  active: {
    backgroundColor: _global.default.colors.lightPaleGrey,
    color: _global.default.colors.blue
  },
  container: {
    alignItems: "center",
    backgroundColor: "transparent",
    cursor: "pointer",
    display: "flex",
    width: "276px"
  },
  disabled: {
    backgroundColor: "transparent",
    color: _global.default.colors.paleGrey,
    cursor: "default"
  },
  iconContainer: {
    alignItems: "center",
    display: "flex",
    height: "24px",
    justifyContent: "center",
    width: "24px"
  },
  innerContainer: {
    alignItems: "center",
    borderRadius: "100px 0px 0px 100px",
    color: _global.default.colors.darkSlateBlue,
    display: "grid",
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "normal",
    gridAutoFlow: "column",
    gridGap: "0px 16px",
    gridTemplateColumns: "24px 1fr",
    lineHeight: "21px",
    padding: "20px 22px",
    width: "inherit"
  },
  onHover: {
    backgroundColor: _global.default.colors.lightPaleGrey,
    color: _global.default.colors.darkSlateBlue
  }
};
var _default = styles;
exports.default = _default;