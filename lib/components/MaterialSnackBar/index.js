"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _MaterialSnackBar = _interopRequireDefault(require("./MaterialSnackBar"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = _MaterialSnackBar.default;
exports.default = _default;