"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  icon: {
    color: _global.default.colors.darkSlateBlue,
    cursor: "pointer",
    fontSize: "24px"
  },
  message: {
    alignItems: "center",
    display: "flex",
    padding: 0
  },
  root: {
    backgroundColor: "transparent",
    boxShadow: "none",
    minWidth: "unset !important",
    width: "fit-content"
  }
};
var _default = styles;
exports.default = _default;