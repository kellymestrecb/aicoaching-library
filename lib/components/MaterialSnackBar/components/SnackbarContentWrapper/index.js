"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _SnackbarContentWrapper = _interopRequireDefault(require("./SnackbarContentWrapper"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = _SnackbarContentWrapper.default;
exports.default = _default;