"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("../../config/global.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  default: {
    color: _global.default.colors.darkSlateBlue,
    fontSize: "24px",
    "&:hover": {
      backgroundColor: _global.default.colors.lightPaleGrey
    }
  },
  disabled: {
    color: "".concat(_global.default.colors.paleGrey, " !important")
  },
  iconRippleChild: {
    backgroundColor: _global.default.colors.darkSlateBlue
  },
  root: {
    display: "block",
    height: "32px",
    padding: 0,
    width: "32px"
  }
};
var _default = styles;
exports.default = _default;