"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var colors = {
  black: "#000000",
  blue: "#0041D9",
  blueyGrey: "#8B95B1",
  darkNavyBlue: "#23366C",
  darkPaleGrey: "#C1C7D7",
  darkSlateBlue: "#23366C",
  lighterSlateBlue: "#F4F5F8",
  lightPaleGrey: "#F1F2F7",
  melon: "#FF7959",
  oliveGreen: "#8CBF59",
  paleGreen: "#E8F2DE",
  paleGrey: "#E8EAF0",
  starlightBlueLighter: "#BFCFF5",
  white: "#FFFFFF",
  paleOrange: "#FFE8D8",
  //TODO: change color name
  orange: "#FF6B00",
  //TODO: change color name
  yellow: "#FFD946",
  //TODO: change color name
  paleYellow: "#FFF9E1" //TODO: change color name

};
var _default = {
  colors: colors
};
exports.default = _default;